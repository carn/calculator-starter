package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
    EditText etNumber1;
    EditText etNumber2;
    TextView result;
    double num1;
    double num2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    public void addNums(View v) {
        if (hasValidInput())
            result.setText(Double.toString(num1 + num2));
    }  //addNums()

    public void subtractNums(View v) {
        if (hasValidInput())
            result.setText(Double.toString(num1 - num2));
    } //subtractNums()

    public void multiplyNums(View v) {
        if (hasValidInput())
            result.setText(Double.toString(num1 * num2));
    } //multiplyNums()

    public void divideNums(View v) {
        if (hasValidInput())
            if (num2 != 0) {
                result.setText(Double.toString(num1 / num2));
            } else {
                result.setText(R.string.divide_by_0);
            }
    } //divideNums()

    public void clear(View v) {
        etNumber1.getText().clear();
        etNumber2.getText().clear();
        result.setText(R.string.answer_hint);
    }

    private boolean hasValidInput() {
        try {
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            return true;
        } catch(NumberFormatException nfe) {
            result.setText(nfe.getLocalizedMessage());
        }
        return false;
    }
}